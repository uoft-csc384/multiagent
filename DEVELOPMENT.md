# Development

In order to test solutions, run:

```bash
PYTHONDONTWRITEBYTECODE=1 python autograder.py \
  --student-code $(ls -m ../../multiagent-solution/multiagent/*.py | tr -d '\n')
```

The `PYTHONDONTWRITEBYTECODE` prevents the creation of "*.pyc" files, which can linger from previous runs.

In order to remove existing "*.pyc" files, run:

```bash
find . -name '*.pyc' --exec rm {} \;
```